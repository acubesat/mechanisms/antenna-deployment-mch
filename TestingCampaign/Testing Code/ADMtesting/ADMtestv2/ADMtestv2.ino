
/**************************************************************************/
/*!
This is a demo for the Adafruit MCP9808 breakout
----> http://www.adafruit.com/products/1782
Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!
*/
/**************************************************************************/

#include <Wire.h>
#include "Adafruit_MCP9808.h"
enum class PIN_SWITCH{
  ONE = 0x0,
  TWO = 0x1,
  THREE = 0x2,
  FOUR = 0x3
  };

const int EN1 = 2;
const int EN2 = 3;

unsigned long timestamp = 0;
// Create the MCP9808 temperature sensor object
Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

bool switches[] = {0, 0, 0, 0};

void setup() {
  pinMode(PIN_SWITCH::ONE, OUTPUT);
  pinMode(PIN_SWITCH::TWO, OUTPUT);
  pinMode(PIN_SWITCH::THREE, OUTPUT);
  pinMode(PIN_SWITCH::FOUR, OUTPUT);
  pinMode(EN1, INPUT_PULLUP);
  pinMode(EN2, INPUT_PULLUP);
  Serial.begin(9600);
 
  if (!tempsensor.begin()) 
  {
    Serial.println("Couldn't find MCP9808!");
    while (1);
  }
  delay(1000);
  float c = tempsensor.readTempC();
}
 
void loop() {
  while digitalRead(startbutton != 0) {
    
  }
  // Read and print out the temperature, then convert to *F
  // float c = tempsensor.readTempC();
  //Serial.print(" C\t"); 
  //delay(250);
 
  //tempsensor.shutdown_wake(1);
  //delay(2000);
  //tempsensor.shutdown_wake(0);
  //unsigned long currentMillis = millis();
  attachInterrupt(digitalPinToInterrupt(PIN_SWITCH::ONE), ISR, CHANGE);
}

void switch1_open() {
  timestamp = millis();
  while (millis() - timestamp < 3){
      bool input = digitalRead(PIN_SWITCH::ONE);
      if (input == false){
        return;
        }
   }
   switches[0] = true;
   check_deployed();
}

void check_deployed(){
  if (switches[0] && switches[1] && switches[2] && switches[3]){
     stop_deployment();
    }
  }
void stop_deployment(){
  digitalWrite (EN1, LOW);
  digitalWrite (@N2, LOW);
}
