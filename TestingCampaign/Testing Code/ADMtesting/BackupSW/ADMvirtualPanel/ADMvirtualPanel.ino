// VirtualPanel H-RollingGraph example - Documentation https://github.com/JaapDanielse/VirtualPanel/wiki/Basic-Examples
// MIT Licence - Copyright (c) 2019 Jaap Danielse - https://github.com/JaapDanielse/VirtualPanel

#include "VirtualPanel.h"
#include <TimeLib.h>
#define DS4 36
#define DS2 23
#define DS3 19
#define DS1 33

bool    InfoVisible = true; // Power status  
byte green = 0;   
const int CURRENT_SENS = 2;
int RAWVALUE = 0;
float CURRENT;
bool    Power1 = false; // Power status
bool    Power2 = false; // Power status
const int EN1 = 18;
const int EN2 = 32;
int EXPERIMENT_STATUS=0;
bool DEPLOYED1 = false;
bool DEPLOYED2 = false;
bool DEPLOYED3 = false;
bool DEPLOYED4 = false;
bool SUCCESS = true;

void setup() 
{
  Panel.begin(); // init port and protocol
  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  pinMode(DS1, INPUT);
  pinMode(DS2, INPUT);
  pinMode(DS3, INPUT);
  pinMode(DS4, INPUT);  
  digitalWrite(EN1,HIGH);
  digitalWrite(EN2,HIGH);
  pinMode(CURRENT_SENS,INPUT); //analogRead(CURRENT_SENS) MAYBE ADD THIS INSTEAD TO SKIP THE 20SEC DEALY ON THE CURRENTVALUE EQUATION
  attachInterrupt(digitalPinToInterrupt(DS1), switch1_opened, RISING);
  attachInterrupt(digitalPinToInterrupt(DS2), switch2_opened, RISING);
  attachInterrupt(digitalPinToInterrupt(DS3), switch3_opened, RISING);
  attachInterrupt(digitalPinToInterrupt(DS4), switch4_opened, RISING);
}

void loop() 
{
  Panel.receive(); // handle panel events form the panel (must be in the loop)
  green  = CURRENT_VALUE(); // generate green "sensor" value 
    if (Power1) digitalWrite(EN1,LOW); else digitalWrite(EN1,HIGH); // my own proces
    if (Power2) digitalWrite(EN2,LOW); else digitalWrite(EN2,HIGH);
  Panel.sendf(Display_3,"%d mA",CURRENT_VALUE()); // Show Value on display_1
  delay(190); // delay
  if (DEPLOYED1 && DEPLOYED2 && DEPLOYED3 && DEPLOYED4 && SUCCESS == true) {
    SUCCESS = false;
    Panel.send(InfoText, "ADM Fully Deployed");   
  }
}

void PanelCallback(vp_channel event) 
{ 
  switch (event) 
  {
    case PanelConnected: // receive panel connected event
      Panel.send(ApplicationName,"ADM TEST"); // set the application name
      Panel.send(Led_1,"$OFF"); // Turn Led_1 off (black)
      Panel.send(Button_1,"Enable1"); // Button_3 visible and set text "on/off"
      Panel.send(Led_2,"$OFF"); // Turn Led_1 off (black)
      Panel.send(Button_2,"Enable2"); // Button_3 visible and set text "on/off"
      Panel.send(GraphLabel_1, "$PINK"); // set label bar color
      Panel.send(GraphLabel_1, "Current \"sensor\"");
      Panel.send(GraphDrawLine, "$3PX");
      Panel.send(GraphCaption_1, "Current \"sensor\" value"); 
      Panel.send(GraphCaption_2, "Current");
      Panel.send(GraphGrid, (int16_t)6); // set graph grid to 15 vertical sections
      Panel.send(Graph, (bool)true); 
      Panel.send(ApplicationName,"ADM Testing Interface"); // set the application name
      Panel.send(Display_1, "$BIG");       // set display 1 (time) to big font
      Panel.send(Display_1, "$BOLD");      // set display 1 (time) to bold 
      Panel.send(Display_2, "$BLACK");     // set display 2 (date) to black
      Panel.send(DynamicDisplay,(int16_t)100);     // enable dynamic display request
      Panel.send(UnixTime, (bool)true);          // request time

      Panel.send(InfoTitle, "Antenna Deployment Mechanism"); // Info Title 
      Panel.send(InfoText, "The graph panel is set visible");
     // Panel.send(DynamicDisplay, (int16_t)100);     // enable dynamic display request
      break;
      
     case UnixTime: // receive (local) time in unix timestamp 
       setTime(Panel.vpr_ulong); // set time using received unsigend long from panel
      break;
          
    case Button_1: // Catch button pressed
      Power1 = !Power1;
      EXPERIMENT_STATUS+=1;
//      float t;
//      if (EXPERIMENT_STATUS%2!=0) t =now(); else Panel.send(MonitorLog,(now()-t));
      if (EXPERIMENT_STATUS%2!=0) 
      {Panel.send(InfoText, "Inner set of resistors ENABLED at");
      Panel.sendf(InfoText, "%02d:%02d:%02d", hour(), minute(), second());}
      else
      {Panel.send(InfoText, "Stopped burning at");
      Panel.sendf(InfoText, "%02d:%02d:%02d", hour(), minute(), second());
      Panel.sendf(InfoText,"reaching %d mA", CURRENT_VALUE());}
      Panel.send(Beep);
      if (Power1)
        Panel.send(Led_1,"$GREEN"); // Turn Led_1 on (red)
      else
        Panel.send(Led_1,"$OFF"); // Turn Led_1 off (black)
      break;
      
     case Button_2: // Catch button pressed
      Power2 = !Power2;
      EXPERIMENT_STATUS+=1;
      if (EXPERIMENT_STATUS%2!=0) 
      {Panel.send(InfoText, "Outer set of resistors ENABLED at");
      Panel.sendf(InfoText, "%02d:%02d:%02d", hour(), minute(), second());}
      else
      {Panel.send(InfoText, "Stopped burning at");
      Panel.sendf(InfoText, "%02d:%02d:%02d", hour(), minute(), second());
      Panel.sendf(InfoText,"reaching %d mA", CURRENT_VALUE());}
      Panel.send(Beep);
      if (Power2)
        Panel.send(Led_2,"$GREEN"); // Turn Led_1 on (red)
      else
        Panel.send(Led_2,"$OFF"); // Turn Led_1 off (black)
      break;
    case DynamicDisplay: // dynamic display request (requested every 500ms)
      Panel.send(GraphValue_3,(byte)map(green, 510,8192,   102, 816)); // map green to grid sections 1 and 2
      Panel.sendf(Display_1,"%02d:%02d:%02d", hour(), minute(), second()); // Output time to display 1
      Panel.sendf(Display_2,"%02d-%02d-%04d", day(), month(), year()); // Output date to display 2
      
      break;
    
    default: break;
  }

  StaticChange();
}

void StaticChange()
{
  Panel.send(Info,InfoVisible); // set info panel visible
}
void switch1_opened()
{if (DEPLOYED1==false){
  DEPLOYED1 = true;
Panel.sendf(InfoText, "Door1 OPENED at ""%02d:%02d:%02d", hour(), minute(), second());
}}
void switch2_opened()
{if (DEPLOYED2==false){
  DEPLOYED2 = true;
Panel.sendf(InfoText, "Door2 OPENED at ""%02d:%02d:%02d", hour(), minute(), second());
}}
void switch3_opened()
{if (DEPLOYED3==false){
  DEPLOYED3 = true;
Panel.sendf(InfoText, "Door3 OPENED at ""%02d:%02d:%02d", hour(), minute(), second());
}}
void switch4_opened()
{if (DEPLOYED4==false){
  DEPLOYED4 = true;
Panel.sendf(InfoText, "Door4 OPENED at ""%02d:%02d:%02d", hour(), minute(), second());
}}
int CURRENT_VALUE() {
  RAWVALUE = analogRead(CURRENT_SENS);
  CURRENT = (RAWVALUE*5.0)/4096;
  CURRENT = CURRENT*1000;
  delay(20);
  return round(CURRENT);
}
