const int EN1 = 18;
const int EN2 = 32;
const int CURRENT_SENS = 2;
int RAWVALUE = 0;
float CURRENT;

void setup() {
  // put your setup code here, to run once:
  pinMode(CURRENT_SENS, INPUT);
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  RAWVALUE = analogRead(CURRENT_SENS);
  CURRENT = (RAWVALUE*5.0)/4096;
  CURRENT = CURRENT*1000;
  delay(200);
  Serial.println(CURRENT);
}
