/*************************************************************
  Download latest Blynk library here:
    https://github.com/blynkkk/blynk-library/releases/latest

  Blynk is a platform with iOS and Android apps to control
  Arduino, Raspberry Pi and the likes over the Internet.
  You can easily build graphic interfaces for all your
  projects by simply dragging and dropping widgets.

    Downloads, docs, tutorials: http://www.blynk.cc
    Sketch generator:           http://examples.blynk.cc
    Blynk community:            http://community.blynk.cc
    Follow us:                  http://www.fb.com/blynkapp
                                http://twitter.com/blynk_app

  Blynk library is licensed under MIT license
  This example code is in public domain.

 *************************************************************
  This example runs directly on ESP8266 chip.

  Note: This requires ESP8266 support package:
    https://github.com/esp8266/Arduino

  Please be sure to select the right ESP8266 module
  in the Tools -> Board menu!

  Change WiFi ssid, pass, and Blynk auth token to run :)
  Feel free to apply it to any other example. It's simple!
 *************************************************************/

/* Comment this out to disable prints and save space */
//#define BLYNK_PRINT Serial

/* Fill-in your Template ID (only if using Blynk.Cloud) */
//#define BLYNK_TEMPLATE_ID   "YourTemplateID"


#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <Wire.h>
#include "Adafruit_MCP9808.h"
enum class PIN_SWITCH{
  ONE = 0x0,
  TWO = 0x1,
  THREE = 0x2,
  FOUR = 0x3
  };

const int EN1 = 2;
const int EN2 = 3;

unsigned long timestamp = 0;
// Create the MCP9808 temperature sensor object
Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

bool switches[] = {0, 0, 0, 0};

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "Py17BMfMDp039JpcnwPc79cJvGUzo0Ou";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "cosmote2";
char pass[] = "########";

BlynkTimer timer;

// This function sends Arduino's up time every second to Virtual Pin (5).
// In the app, Widget's reading frequency should be set to PUSH. This means
// that you define how often to send data to Blynk App.
void myTimerEvent()
{
  // You can send any value at any time.
  // Please don't send more that 10 values per second. millis() / 1000
  Blynk.virtualWrite(V5,c);
}

BLYNK_WRITE(V1)
{
  int experiment_status = param.asInt(); // assigning incoming value from pin V1 to a variable
  // process received value
}
BLYNK_WRITE(V5)
{
  unsigned long burn_duration = param.asInt()*10; // assigning incoming value from pin V1 to a variable
  // process received value
}

void setup()
{
  // Debug console
  Serial.begin(9600);
  pinMode(PIN_SWITCH::ONE, OUTPUT);
  pinMode(PIN_SWITCH::TWO, OUTPUT);
  pinMode(PIN_SWITCH::THREE, OUTPUT);
  pinMode(PIN_SWITCH::FOUR, OUTPUT);
  pinMode(EN1, INPUT_PULLUP);
  pinMode(EN2, INPUT_PULLUP);

 // if (!tempsensor.begin()) 
  //{
    //Serial.println("Couldn't find MCP9808!");
   // while (1);
  //}
  delay(100);
  tempsensor.setResolution(3);
  tempsensor.wake();
  float c = tempsensor.readTempC();
  delay(100);
  
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(1000L, myTimerEvent);
}

void loop()
{
  
  Blynk.run();
  timer.run();
  attachInterrupt(digitalPinToInterrupt(PIN_SWITCH::ONE), deployment1, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SWITCH::TWO), deployment2, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SWITCH::THREE), deployment3, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SWITCH::FOUR), deployment4, RISING);
  if (experiment_status != 1){
    continue;
  }
  //unsigned long currentmillis = millis();
  digitalWrite(EN1, HIGH);
  unsigned long timestamp = millis();
  while (millis() - timestamp < burn_duration){
    digitalWrite(EN1, LOW);
    digitalWrite(EN2, HIGH);)
   }
  digitalWrite(EN2, LOW);
  
}
void deployment1() {
  Serial.print ("Deployment on SIDE 1");
  unsigned long currentmillis = millis();
  Serial.print((currentmillis - timestamp)/1000);
}
