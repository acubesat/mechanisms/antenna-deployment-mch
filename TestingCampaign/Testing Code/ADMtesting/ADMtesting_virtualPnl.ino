
// VirtualPanel A-Button example - Documentation https://github.com/JaapDanielse/VirtualPanel/wiki/Basic-Examples
// MIT Licence - Copyright (c) 2019 Jaap Danielse - https://github.com/JaapDanielse/VirtualPanel

#include "VirtualPanel.h"
#include <dht.h>

#define enablepin1 5
#define enablepin2 6 //D#
#define tempPin 8
dht DHT;


bool    Power1 = false; // Power status
int16_t Value = 0;  // Integer variabel
const int DeploySwitch[] = {2,3,7,8}; //D#

void setup() 
{
  Serial.begin(9600);
  Panel.begin(); // init port and protocol
  pinMode(enable1, OUTPUT);
  pinMode(enable2, OUTPUT);
  digitalWrite(enable1,LOW);
  digitalWrite(enable2,LOW);
  for (byte i = 0; i < 4; i++)
   pinMode (DeploySwitch[i], INPUT);
  
}

void loop() 
{
  Panel.receive(); // handle panel events form the panel (must be in the loop)
  //if (Power) Value = random(1,100); else Value = 0; // my own proces
  for (byte i = 0; i < 4; i++)
   if digitalRead(DeploySwitch[i]) != 0
  int readData = DHT.read22(tempPin); 
  float t = DHT.temperature;
  float h = DHT.humidity;
  Panel.sendf(Display_1,"%d",t); // Show Value on display_1
  Panel.sendf(Display_1,"%d",h);
  delay(500); // delay 
}

void PanelCallback(vp_channel event) 
{ 
  switch (event) 
  {
    case PanelConnected: // receive panel connected event
      Panel.send(ApplicationName,"ADM Testing"); // set the application name
      Panel.send(Led_1,"$OFF"); // Turn Led_1 off (black)
      Panel.send(Button_3,"on/off"); // Button_3 visible and set text "on/off"
      break;

    case Button_3: // Catch button pressed
      Power = !Power;
      if (Power)
        Panel.send(Led_1,"$RED"); // Turn Led_1 on (red)
        digitalWrite(enable1,HIGH);
      else
        Panel.send(Led_1,"$OFF"); // Turn Led_1 off (black)
        digitalWrite(enable1,LOW);
      break;

      default: break;

  }
}
