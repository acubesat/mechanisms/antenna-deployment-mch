
/**************************************************************************/
/*!
This is a demo for the Adafruit MCP9808 breakout
----> http://www.adafruit.com/products/1782
Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!
*/
/**************************************************************************/

//#include <Wire.h>
//#include "Adafruit_MCP9808.h"
#define DEBOUNCE_TIME 3
#define PIN1 36
#define PIN2 23
#define PIN3 19
#define PIN4 33
#define PINFIRE 2 //VIRTUAL SWITCH TO START EXPERIMENT

const int EN1 = 18;
const int EN2 = 32;
const int CURRENT_SENS = 2;
int RAWVALUE = 0;
float CURRENT;

void switch1_opened();
void switch1_closed();

unsigned long timestamp = 0;
// Create the MCP9808 temperature sensor object
//Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

bool switches[] = {0, 0, 0, 0};

bool switch_firing[] = {0, 0, 0, 0};
bool switch_opened[] = {0, 0, 0, 0};
bool switch_deployed[] = {0, 0, 0, 0};
unsigned long switch_timestamps[] = {0, 0, 0, 0};

bool fire_switch_pressed = 0;
bool fire_switch_pressed_timestamp = 0;


void setup() {
  pinMode(PIN1, INPUT);
  pinMode(PIN2, INPUT_PULLUP);
  pinMode(PIN3, INPUT_PULLUP);
  pinMode(PIN4, INPUT_PULLUP);
  pinMode(CURRENT_SENS, INPUT);
  pinMode(PINFIRE, INPUT_PULLUP);
  
  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  digitalWrite (EN1, LOW);
  digitalWrite (EN2, LOW);
  // TODO: See which state corresponds to rising and which to falling interrupt
  attachInterrupt(digitalPinToInterrupt(PIN1), switch1_opened, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN1), switch1_closed, FALLING);
  attachInterrupt(digitalPinToInterrupt(PINFIRE), switchfire_pressed, FALLING);
  attachInterrupt(digitalPinToInterrupt(PINFIRE), switchfire_unpressed, RISING);
  
  Serial.begin(115200);
  Serial.print("Starting");
  //if (!tempsensor.begin()) 
  //{
   // Serial.println("Couldn't find MCP9808!");
    //while (1);
  //}
  //delay(1000);
  //float c = tempsensor.readTempC();
}
 
void loop() {

  // Read and print out the temperature, then convert to *F
  // float c = tempsensor.readTempC();
  //Serial.print(" C\t"); 
  //delay(250);
 
  //tempsensor.shutdown_wake(1);
  //delay(2000);
  //tempsensor.shutdown_wake(0);
  //unsigned long currentMillis = millis();

  for (int switch_n = 0; switch_n < 4; switch_n++){
    Serial.println(CURRENT_VALUE());
    delay(2);
    if (switch_opened[switch_n] && (millis() - switch_timestamps[switch_n] < DEBOUNCE_TIME)){
        switch_deployed[switch_n] = 1;
        // TODO: LOG switch_n opened!!!
        Serial.println("Switch opened: ");
        Serial.print(switch_n);
        Serial.print(CURRENT_VALUE());
      }  
  }

}

void switchfire_pressed() {
  fire_switch_pressed_timestamp = millis();
}

void switchfire_unpressed() {
  if (fire_switch_pressed_timestamp - millis() < 20){
      digitalWrite (EN1, HIGH);
    }
}
 
 
void switch1_opened() {
  if (switch_deployed[0] == 1) return;
  switch_opened[0]  = 1;
  switch_timestamps[0] = millis();
}

void switch1_closed() {
  if (switch_deployed[0] == 1) return;
  switch_opened[0]  = 0; 
}

void switch2_opened() {
  if (switch_deployed[1] == 1) return;
  switch_opened[1]  = 1;
  switch_timestamps[1] = millis();
}

void switch2_closed() {
  if (switch_deployed[1] == 1) return;
  switch_opened[1]  = 0; 
}

void switch3_opened() {
  if (switch_deployed[2] == 1) return;
  switch_opened[2]  = 1;
  switch_timestamps[2] = millis();
}

void switch3_closed() {
  if (switch_deployed[2] == 1) return;
  switch_opened[2]  = 0; 
}

void switch4_opened() {
  if (switch_deployed[3] == 1) return;
  switch_opened[3]  = 1;
  switch_timestamps[3] = millis();
}

void switch4_closed() {
  if (switch_deployed[3] == 1) return;
  switch_opened[3]  = 0; 
}

float CURRENT_VALUE() {
  RAWVALUE = analogRead(CURRENT_SENS);
  CURRENT = (RAWVALUE*5.0)/4096;
  CURRENT = CURRENT*1000;
  delay(200);
  return CURRENT;
}
