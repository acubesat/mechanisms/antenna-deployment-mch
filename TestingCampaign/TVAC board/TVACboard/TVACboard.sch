EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 2311765-1:2311765-1 J4
U 1 1 61A7204B
P 7750 2100
F 0 "J4" H 8794 2146 50  0000 L CNN
F 1 "2311765-1" H 8050 2000 50  0000 L CNN
F 2 "SamacSys_Parts:23117651" H 8600 2400 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F2311765%7FD%7Fpdf%7FEnglish%7FENG_CD_2311765_D.pdf%7F2311765-1" H 8600 2300 50  0001 L CNN
F 4 "AMPLIMITE HD22 Board Mount Connectors: AMPL REC, HD20, R/A, 9P, S/L, .318" H 8600 2200 50  0001 L CNN "Description"
F 5 "12.8" H 8600 2100 50  0001 L CNN "Height"
F 6 "571-2311765-1" H 8600 2000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/2311765-1?qs=rrS6PyfT74ebQi5fQFugmA%3D%3D" H 8600 1900 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 8600 1800 50  0001 L CNN "Manufacturer_Name"
F 9 "2311765-1" H 8600 1700 50  0001 L CNN "Manufacturer_Part_Number"
	1    7750 2100
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:504050-1091 J5
U 1 1 61A75089
P 8600 2550
F 0 "J5" V 9004 2678 50  0000 L CNN
F 1 "504050-1091" V 9050 2100 50  0000 L CNN
F 2 "SamacSys_Parts:504050-1091" H 9350 2650 50  0001 L CNN
F 3 "https://datasheet.datasheetarchive.com/originals/distributors/Datasheets_SAMA/78973205e9f4d17ddd54ea229574413e.pdf" H 9350 2550 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 10way Molex 504050 Series, 1.5mm Pitch 10 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 9350 2450 50  0001 L CNN "Description"
F 5 "" H 9350 2350 50  0001 L CNN "Height"
F 6 "538-504050-1091" H 9350 2250 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-1091?qs=bvCPb%252BE7ys3Pdh2ki4M4Kw%3D%3D" H 9350 2150 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 9350 2050 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-1091" H 9350 1950 50  0001 L CNN "Manufacturer_Part_Number"
	1    8600 2550
	0    1    -1   0   
$EndComp
Wire Wire Line
	7450 2600 8200 2600
Wire Wire Line
	8200 2600 8200 2550
Wire Wire Line
	8300 2550 8300 2650
Wire Wire Line
	8300 2650 7350 2650
Wire Wire Line
	7350 2650 7350 2600
Wire Wire Line
	7250 2600 7250 2700
Wire Wire Line
	7250 2700 8400 2700
Wire Wire Line
	8400 2700 8400 2550
Wire Wire Line
	8500 2550 8500 2750
Wire Wire Line
	8500 2750 7150 2750
Wire Wire Line
	7050 2600 7050 2800
Wire Wire Line
	8600 2800 8600 2550
Wire Wire Line
	8300 1650 8300 1600
Wire Wire Line
	8300 1600 7450 1600
Wire Wire Line
	7350 1600 7350 1550
Wire Wire Line
	7350 1550 8400 1550
Wire Wire Line
	8400 1550 8400 1650
Wire Wire Line
	8500 1650 8500 1500
Wire Wire Line
	8500 1500 7250 1500
Wire Wire Line
	7250 1500 7250 1600
Wire Wire Line
	7150 1600 7150 1450
Wire Wire Line
	7150 1450 8600 1450
Wire Wire Line
	8600 1450 8600 1650
NoConn ~ 8200 1650
NoConn ~ 7750 2100
NoConn ~ 6750 2100
$Comp
L 2311765-1:2311765-1 J2
U 1 1 61AFE41C
P 3300 2100
F 0 "J2" H 4344 2146 50  0000 L CNN
F 1 "2311765-1" H 3600 2000 50  0000 L CNN
F 2 "SamacSys_Parts:23117651" H 4150 2400 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F2311765%7FD%7Fpdf%7FEnglish%7FENG_CD_2311765_D.pdf%7F2311765-1" H 4150 2300 50  0001 L CNN
F 4 "AMPLIMITE HD22 Board Mount Connectors: AMPL REC, HD20, R/A, 9P, S/L, .318" H 4150 2200 50  0001 L CNN "Description"
F 5 "12.8" H 4150 2100 50  0001 L CNN "Height"
F 6 "571-2311765-1" H 4150 2000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/2311765-1?qs=rrS6PyfT74ebQi5fQFugmA%3D%3D" H 4150 1900 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 4150 1800 50  0001 L CNN "Manufacturer_Name"
F 9 "2311765-1" H 4150 1700 50  0001 L CNN "Manufacturer_Part_Number"
	1    3300 2100
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:504050-1091 J1
U 1 1 61AFE428
P 2450 1650
F 0 "J1" V 2854 1778 50  0000 L CNN
F 1 "504050-1091" V 2900 1200 50  0000 L CNN
F 2 "SamacSys_Parts:504050-1091" H 3200 1750 50  0001 L CNN
F 3 "https://datasheet.datasheetarchive.com/originals/distributors/Datasheets_SAMA/78973205e9f4d17ddd54ea229574413e.pdf" H 3200 1650 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 10way Molex 504050 Series, 1.5mm Pitch 10 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 3200 1550 50  0001 L CNN "Description"
F 5 "" H 3200 1450 50  0001 L CNN "Height"
F 6 "538-504050-1091" H 3200 1350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-1091?qs=bvCPb%252BE7ys3Pdh2ki4M4Kw%3D%3D" H 3200 1250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 3200 1150 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-1091" H 3200 1050 50  0001 L CNN "Manufacturer_Part_Number"
	1    2450 1650
	0    -1   1    0   
$EndComp
Wire Wire Line
	3600 1600 2850 1600
Wire Wire Line
	2850 1600 2850 1650
Wire Wire Line
	2750 1650 2750 1550
Wire Wire Line
	2750 1550 3700 1550
Wire Wire Line
	3700 1550 3700 1600
Wire Wire Line
	3800 1600 3800 1500
Wire Wire Line
	3800 1500 2650 1500
Wire Wire Line
	2650 1500 2650 1650
Wire Wire Line
	2550 1650 2550 1450
Wire Wire Line
	2550 1450 3900 1450
Wire Wire Line
	4000 1600 4000 1400
Wire Wire Line
	2450 1400 2450 1650
Wire Wire Line
	2750 2550 2750 2600
Wire Wire Line
	2750 2600 3600 2600
Wire Wire Line
	3700 2600 3700 2650
Wire Wire Line
	3700 2650 2650 2650
Wire Wire Line
	2650 2650 2650 2550
Wire Wire Line
	2550 2550 2550 2700
Wire Wire Line
	2550 2700 3800 2700
Wire Wire Line
	3800 2700 3800 2600
Wire Wire Line
	3900 2600 3900 2750
Wire Wire Line
	3900 2750 2450 2750
Wire Wire Line
	2450 2750 2450 2550
NoConn ~ 2850 2550
NoConn ~ 3300 2100
NoConn ~ 4300 2100
Wire Wire Line
	3900 1450 3900 1600
Wire Wire Line
	4000 1400 2450 1400
Text Label 5050 1900 2    50   ~ 0
5a
Wire Wire Line
	7150 2600 7150 2750
$Comp
L SamacSys_Parts:203564-2017 J3
U 1 1 61A8A45B
P 5550 1400
F 0 "J3" V 5369 1400 50  0000 C CNN
F 1 "203564-2017" V 5460 1400 50  0000 C CNN
F 2 "SamacSys_Parts:2035642017" H 6900 1700 50  0001 L CNN
F 3 "https://www.molex.com/pdm_docs/sd/2035642017_sd.pdf" H 6900 1600 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT DR Vt 20CKT W/PL Au0.38" H 6900 1500 50  0001 L CNN "Description"
F 5 "7" H 6900 1400 50  0001 L CNN "Height"
F 6 "538-203564-2017" H 6900 1300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203564-2017?qs=F5EMLAvA7IAHUbz5s3aACQ%3D%3D" H 6900 1200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 6900 1100 50  0001 L CNN "Manufacturer_Name"
F 9 "203564-2017" H 6900 1000 50  0001 L CNN "Manufacturer_Part_Number"
	1    5550 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	7050 2800 8600 2800
Text Label 5050 1800 2    50   ~ 0
3a
Text Label 5050 1700 2    50   ~ 0
1a
Text Label 5050 2000 2    50   ~ 0
7a
Text Label 5050 2600 2    50   ~ 0
19a
Text Label 5050 2300 2    50   ~ 0
13a
Text Label 5050 2400 2    50   ~ 0
15a
Text Label 5050 2500 2    50   ~ 0
17a
Text Label 5050 2100 2    50   ~ 0
9a
Text Label 6050 1700 0    50   ~ 0
2b
Text Label 6050 1800 0    50   ~ 0
4a
Text Label 6050 1900 0    50   ~ 0
6b
Text Label 6050 2000 0    50   ~ 0
8b
Text Label 6050 2100 0    50   ~ 0
10b
Text Label 6050 2300 0    50   ~ 0
14b
Text Label 6050 2400 0    50   ~ 0
16b
Text Label 6050 2500 0    50   ~ 0
18b
Text Label 6050 2600 0    50   ~ 0
20b
Text Label 3600 1600 2    50   ~ 0
1a
Text Label 3600 2600 2    50   ~ 0
3a
Text Label 3700 1550 2    50   ~ 0
5a
Text Label 3700 2600 2    50   ~ 0
7a
Text Label 3800 1500 2    50   ~ 0
9a
Text Label 3800 2700 2    50   ~ 0
13a
Text Label 3900 1450 2    50   ~ 0
15a
Text Label 3900 2750 0    50   ~ 0
17a
Text Label 4000 1400 2    50   ~ 0
19a
Text Label 7050 2800 3    50   ~ 0
2b
Text Label 7150 1450 1    50   ~ 0
4a
Text Label 7150 2750 3    50   ~ 0
6b
Text Label 7250 1500 1    50   ~ 0
8b
Text Label 7250 2700 3    50   ~ 0
10b
Text Label 7350 1550 1    50   ~ 0
14b
Text Label 7350 2650 3    50   ~ 0
16b
Text Label 7450 1600 1    50   ~ 0
18b
Text Label 7450 2600 3    50   ~ 0
20b
$EndSCHEMATC
