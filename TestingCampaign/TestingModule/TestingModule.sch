EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x19_Male J1
U 1 1 62D64092
P 4100 3250
F 0 "J1" H 4208 4331 50  0000 C CNN
F 1 "Conn_01x19_Male" H 4208 4240 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x19_P2.54mm_Vertical" H 4100 3250 50  0001 C CNN
F 3 "~" H 4100 3250 50  0001 C CNN
	1    4100 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x19_Male J3
U 1 1 62D66482
P 4900 3250
F 0 "J3" H 5008 4331 50  0000 C CNN
F 1 "Conn_01x19_Male" H 5008 4240 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x19_P2.54mm_Vertical" H 4900 3250 50  0001 C CNN
F 3 "~" H 4900 3250 50  0001 C CNN
	1    4900 3250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x19_Male J4
U 1 1 62D6C23E
P 6050 3250
F 0 "J4" H 6158 4331 50  0000 C CNN
F 1 "Conn_01x19_Male" H 6158 4240 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x19_P2.54mm_Vertical" H 6050 3250 50  0001 C CNN
F 3 "~" H 6050 3250 50  0001 C CNN
	1    6050 3250
	-1   0    0    -1  
$EndComp
$Comp
L Connector:DB37_Male J2
U 1 1 62D6E190
P 4900 1300
F 0 "J2" V 5217 1313 50  0000 C CNN
F 1 "DB37_Male" V 5126 1313 50  0000 C CNN
F 2 "Connector_Dsub:DSUB-37_Male_Horizontal_P2.77x2.84mm_EdgePinOffset4.94mm_Housed_MountingHolesOffset7.48mmMOD" H 4900 1300 50  0001 C CNN
F 3 " ~" H 4900 1300 50  0001 C CNN
	1    4900 1300
	0    -1   -1   0   
$EndComp
Text Label 5850 3650 0    50   ~ 0
GND
Text Label 5100 3650 0    50   ~ 0
GND
Text Label 4300 3650 0    50   ~ 0
GND
Text Label 4600 4600 0    50   ~ 0
GND
$Comp
L power:GND #PWR01
U 1 1 62D7E31C
P 4600 4600
F 0 "#PWR01" H 4600 4350 50  0001 C CNN
F 1 "GND" H 4605 4427 50  0000 C CNN
F 2 "" H 4600 4600 50  0001 C CNN
F 3 "" H 4600 4600 50  0001 C CNN
	1    4600 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 62D7F319
P 5100 4500
F 0 "R1" H 5159 4546 50  0000 L CNN
F 1 "R_Small" H 5159 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5100 4500 50  0001 C CNN
F 3 "~" H 5100 4500 50  0001 C CNN
	1    5100 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 62D7F5FF
P 5600 4500
F 0 "R2" H 5659 4546 50  0000 L CNN
F 1 "R_Small" H 5659 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 5600 4500 50  0001 C CNN
F 3 "~" H 5600 4500 50  0001 C CNN
	1    5600 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small Rf1
U 1 1 62DAC739
P 6800 4300
F 0 "Rf1" H 6859 4346 50  0000 L CNN
F 1 "R_Small" H 6859 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 6800 4300 50  0001 C CNN
F 3 "~" H 6800 4300 50  0001 C CNN
	1    6800 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 62DACE31
P 7850 4300
F 0 "R5" H 7909 4346 50  0000 L CNN
F 1 "R_Small" H 7909 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7850 4300 50  0001 C CNN
F 3 "~" H 7850 4300 50  0001 C CNN
	1    7850 4300
	1    0    0    -1  
$EndComp
Text Label 6850 2800 3    50   ~ 0
Vcc
Text Label 6800 4400 3    50   ~ 0
IN-
Text Label 6350 2800 3    50   ~ 0
RS+
Text Label 5600 4400 0    50   ~ 0
JP1
Text Label 6450 2800 3    50   ~ 0
GND
$Comp
L Connector:Conn_01x06_Male J5
U 1 1 62DB13DD
P 6650 2600
F 0 "J5" V 6712 2844 50  0000 L CNN
F 1 "Conn_01x06_Male" V 6803 2844 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 6650 2600 50  0001 C CNN
F 3 "~" H 6650 2600 50  0001 C CNN
	1    6650 2600
	0    1    1    0   
$EndComp
Text Label 6750 2800 3    50   ~ 0
GND
Text Label 9050 2250 0    50   ~ 0
GND
Text Label 9050 2350 0    50   ~ 0
GND
Text Label 6650 2800 3    50   ~ 0
IO1
Text Label 6550 2800 3    50   ~ 0
IO2
$Comp
L SamacSys_Parts:CD74HC02M IC1
U 1 1 62DBAB06
P 8650 4250
F 0 "IC1" H 9150 4515 50  0000 C CNN
F 1 "CD74HC02M" H 9150 4424 50  0000 C CNN
F 2 "SamacSys_Parts:DIP794W56P254L1905H533Q14N" H 9500 4350 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/CD74HC02" H 9500 4250 50  0001 L CNN
F 4 "High-Speed CMOS Logic Quad Two-Input NOR Gate" H 9500 4150 50  0001 L CNN "Description"
F 5 "1.75" H 9500 4050 50  0001 L CNN "Height"
F 6 "595-CD74HC02M" H 9500 3950 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/CD74HC02M?qs=YxwvVplHM%2Fn%252BnTOMw1hObA%3D%3D" H 9500 3850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 9500 3750 50  0001 L CNN "Manufacturer_Name"
F 9 "CD74HC02M" H 9500 3650 50  0001 L CNN "Manufacturer_Part_Number"
	1    8650 4250
	1    0    0    -1  
$EndComp
Text Label 5850 3550 0    50   ~ 0
IO1
Text Label 5850 3450 0    50   ~ 0
IO2
Text Label 5850 3750 0    50   ~ 0
3V3
Text Label 5100 3550 0    50   ~ 0
IO1
Text Label 5100 3450 0    50   ~ 0
IO2
Text Label 5100 3750 0    50   ~ 0
3V3
Text Label 4300 2450 0    50   ~ 0
SW4
Text Label 4300 2550 0    50   ~ 0
PHT
Text Label 4300 2950 0    50   ~ 0
SW1
Text Label 5100 2850 0    50   ~ 0
SW3
Text Label 5100 2950 0    50   ~ 0
EN1
Text Label 5850 3350 0    50   ~ 0
OUT
Text Label 5100 3350 0    50   ~ 0
OUT
Text Label 7850 4400 3    50   ~ 0
3V3
Text Label 7850 4200 1    50   ~ 0
IO1
Text Label 8650 4550 2    50   ~ 0
3V3
Text Label 8650 4250 2    50   ~ 0
OUT
Text Label 8650 4350 2    50   ~ 0
IN-
Text Label 8650 4450 2    50   ~ 0
IN+
Text Label 9650 4550 0    50   ~ 0
GND
Text Label 6800 4200 1    50   ~ 0
OUT
$Comp
L Device:R_Small Cf1
U 1 1 62DC18F9
P 6800 4900
F 0 "Cf1" H 6859 4946 50  0000 L CNN
F 1 "R_Small" H 6859 4855 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" H 6800 4900 50  0001 C CNN
F 3 "~" H 6800 4900 50  0001 C CNN
	1    6800 4900
	1    0    0    -1  
$EndComp
Text Label 6800 5000 3    50   ~ 0
IN-
Text Label 6800 4800 1    50   ~ 0
OUT
Text Label 4300 3750 0    50   ~ 0
Vin
Text Label 5100 4600 3    50   ~ 0
Vin
Text Label 5100 4400 1    50   ~ 0
RS+
Text Label 5600 4600 3    50   ~ 0
EXTR+
Text Label 9050 3250 0    50   ~ 0
EXTR+
Wire Wire Line
	9050 3250 9050 3350
Text Label 5100 2350 0    50   ~ 0
SW2
Text Label 5850 2350 0    50   ~ 0
SW2
Wire Wire Line
	5600 4400 5100 4400
NoConn ~ 8650 4650
NoConn ~ 8650 4750
NoConn ~ 8650 4850
NoConn ~ 9650 4850
NoConn ~ 9650 4750
NoConn ~ 9650 4650
NoConn ~ 9650 4450
NoConn ~ 9650 4350
NoConn ~ 9650 4250
Wire Wire Line
	5100 3850 5850 3850
Wire Wire Line
	5850 3950 5100 3950
Wire Wire Line
	5850 4050 5100 4050
Wire Wire Line
	5100 4150 5850 4150
Wire Wire Line
	5100 3750 5850 3750
Wire Wire Line
	5100 3250 5850 3250
Wire Wire Line
	5100 3150 5850 3150
Wire Wire Line
	5100 3050 5850 3050
Wire Wire Line
	5100 2950 5850 2950
Wire Wire Line
	5850 2850 5100 2850
Wire Wire Line
	5100 2750 5850 2750
Wire Wire Line
	5850 2650 5100 2650
Wire Wire Line
	5100 2550 5850 2550
Wire Wire Line
	5850 2450 5100 2450
Wire Wire Line
	5100 2350 5850 2350
Wire Wire Line
	5850 3350 5100 3350
Wire Wire Line
	5100 3450 5850 3450
Wire Wire Line
	5850 3550 5100 3550
Wire Wire Line
	5100 3650 5850 3650
NoConn ~ 4300 2650
NoConn ~ 4300 2750
NoConn ~ 4300 3050
NoConn ~ 4300 3150
NoConn ~ 4300 3250
NoConn ~ 4300 3350
NoConn ~ 4300 3450
NoConn ~ 4300 3550
NoConn ~ 4300 3950
NoConn ~ 4300 4050
NoConn ~ 4300 4150
Wire Wire Line
	4300 3750 4300 3850
NoConn ~ 4300 2350
NoConn ~ 3100 1600
NoConn ~ 3200 1600
NoConn ~ 3300 1600
NoConn ~ 3400 1600
NoConn ~ 3500 1600
NoConn ~ 3600 1600
NoConn ~ 3700 1600
NoConn ~ 3800 1600
NoConn ~ 3900 1600
NoConn ~ 4200 1600
NoConn ~ 5000 1600
NoConn ~ 5100 1600
NoConn ~ 5200 1600
NoConn ~ 5300 1600
NoConn ~ 5400 1600
NoConn ~ 5500 1600
NoConn ~ 5600 1600
NoConn ~ 5700 1600
NoConn ~ 5800 1600
NoConn ~ 5900 1600
NoConn ~ 6000 1600
NoConn ~ 6100 1600
NoConn ~ 6200 1600
NoConn ~ 6300 1600
NoConn ~ 6400 1600
NoConn ~ 6500 1600
NoConn ~ 6600 1600
NoConn ~ 6700 1600
Text Label 8650 4450 0    50   ~ 0
GND
Text Label 4300 2550 2    50   ~ 0
IN-
$Comp
L INA290A5QDCKRQ1:INA290A5QDCKRQ1 IC2
U 1 1 62DC8416
P 9550 2650
F 0 "IC2" H 10050 2915 50  0000 C CNN
F 1 "INA290A5QDCKRQ1" H 10050 2824 50  0000 C CNN
F 2 "SamacSys_Parts:SOT65P210X110-5N" H 10400 2750 50  0001 L CNN
F 3 "" H 10400 2650 50  0001 L CNN
F 4 "Current Sense Amplifiers AEC-Q100, 2.7-V to 120-V, 1.1-MHz, ultra-precise current sense amplifier in small (SC-70) package 5-SC70 -40 to 125" H 10400 2550 50  0001 L CNN "Description"
F 5 "1.1" H 10400 2450 50  0001 L CNN "Height"
F 6 "595-INA290A5QDCKRQ1" H 10400 2350 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/INA290A5QDCKRQ1?qs=eP2BKZSCXI4fJLGjUD5iZg%3D%3D" H 10400 2250 50  0001 L CNN "Mouser Price/Stock"
F 8 "Texas Instruments" H 10400 2150 50  0001 L CNN "Manufacturer_Name"
F 9 "INA290A5QDCKRQ1" H 10400 2050 50  0001 L CNN "Manufacturer_Part_Number"
	1    9550 2650
	1    0    0    -1  
$EndComp
Text Label 9550 2850 2    50   ~ 0
EXTR+
Text Label 9550 2750 2    50   ~ 0
GND
Text Label 9550 2650 2    50   ~ 0
IO1
Text Label 10550 2750 3    50   ~ 0
Vcc
Text Label 10550 2650 1    50   ~ 0
RS+
Wire Wire Line
	10550 2650 10600 2650
Wire Wire Line
	10600 2650 10600 2550
Wire Wire Line
	10600 2550 10800 2550
$Comp
L WSHP2818R0500FEB:WSHP2818R0500FEB R3
U 1 1 62DCE6E8
P 10800 2550
F 0 "R3" V 11104 2638 50  0000 L CNN
F 1 "WSHP2818R0500FEB" V 11195 2638 50  0000 L CNN
F 2 "SamacSys_Parts:RESC7146X175N" H 11350 2600 50  0001 L CNN
F 3 "https://www.vishay.com/docs/30347/wshp2818.pdf" H 11350 2500 50  0001 L CNN
F 4 "50m 2818 Metal Strip Surface Mount Fixed Resistor +/-1% 10W WSHP2818R0500FEB" H 11350 2400 50  0001 L CNN "Description"
F 5 "1.75" H 11350 2300 50  0001 L CNN "Height"
F 6 "71-WSHP2818R0500FEB" H 11350 2200 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Vishay/WSHP2818R0500FEB?qs=r5DSvlrkXmJsq5XfyE%252BVvA%3D%3D" H 11350 2100 50  0001 L CNN "Mouser Price/Stock"
F 8 "Vishay" H 11350 2000 50  0001 L CNN "Manufacturer_Name"
F 9 "WSHP2818R0500FEB" H 11350 1900 50  0001 L CNN "Manufacturer_Part_Number"
	1    10800 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 2750 10550 3250
Wire Wire Line
	10550 3250 10800 3250
$Comp
L Device:R_Small R4
U 1 1 62DD395F
P 11000 2900
F 0 "R4" H 11059 2946 50  0000 L CNN
F 1 "R_Small" H 11059 2855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 11000 2900 50  0001 C CNN
F 3 "~" H 11000 2900 50  0001 C CNN
	1    11000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 3000 11000 3250
Wire Wire Line
	11000 3250 10800 3250
Connection ~ 10800 3250
Wire Wire Line
	10800 2550 11000 2550
Wire Wire Line
	11000 2550 11000 2800
Connection ~ 10800 2550
Text Label 4400 1600 3    50   ~ 0
SW4
Text Label 4600 1600 3    50   ~ 0
PHT
Text Label 4800 1600 3    50   ~ 0
EN1
Text Label 4000 1600 3    50   ~ 0
Vcc
Text Label 4900 1600 3    50   ~ 0
SW2
Text Label 4700 1600 3    50   ~ 0
SW1
Text Label 4500 1600 3    50   ~ 0
EN2
Text Label 4300 1600 3    50   ~ 0
SW3
Text Label 4100 1600 3    50   ~ 0
GND
$Comp
L Connector:4P2C J6
U 1 1 62DB80EC
P 8650 2350
F 0 "J6" H 8707 2817 50  0000 C CNN
F 1 "4P2C" H 8707 2726 50  0000 C CNN
F 2 "Connector:CalTest_CT3151" V 8650 2400 50  0001 C CNN
F 3 "~" V 8650 2400 50  0001 C CNN
	1    8650 2350
	1    0    0    -1  
$EndComp
$Comp
L Connector:4P2C J7
U 1 1 62DB775C
P 8650 3350
F 0 "J7" H 8707 3817 50  0000 C CNN
F 1 "4P2C" H 8707 3726 50  0000 C CNN
F 2 "Connector:CalTest_CT3151" V 8650 3400 50  0001 C CNN
F 3 "~" V 8650 3400 50  0001 C CNN
	1    8650 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R7
U 1 1 62F2A243
P 6150 3050
F 0 "R7" H 6209 3096 50  0000 L CNN
F 1 "R_Small" H 6209 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6150 3050 50  0001 C CNN
F 3 "~" H 6150 3050 50  0001 C CNN
	1    6150 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 62F2B43D
P 4500 2850
F 0 "R6" H 4559 2896 50  0000 L CNN
F 1 "R_Small" H 4559 2805 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 4500 2850 50  0001 C CNN
F 3 "~" H 4500 2850 50  0001 C CNN
	1    4500 2850
	0    1    1    0   
$EndComp
Text Label 4300 2850 0    50   ~ 0
EN2
Wire Wire Line
	4400 2850 4300 2850
Wire Wire Line
	6150 2950 5850 2950
Connection ~ 5850 2950
Text Label 6150 3150 3    50   ~ 0
3V3
Text Label 4600 2850 0    50   ~ 0
3V3
$EndSCHEMATC
