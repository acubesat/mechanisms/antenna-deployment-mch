EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x10_Female J3
U 1 1 61CB2BCD
P 6050 2450
F 0 "J3" H 5942 1725 50  0000 C CNN
F 1 "Conn_01x10_Female" H 5942 1816 50  0000 C CNN
F 2 "Connector_Molex:Molex_CLIK-Mate_502585-1070_1x10-1MP_P1.50mm_Horizontal" H 6050 2450 50  0001 C CNN
F 3 "~" H 6050 2450 50  0001 C CNN
	1    6050 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Counter_Clockwise J1
U 1 1 61CC0554
P 4200 2400
F 0 "J1" H 4250 2717 50  0000 C CNN
F 1 "Conn_02x03_Counter_Clockwise" H 4250 2626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4200 2400 50  0001 C CNN
F 3 "~" H 4200 2400 50  0001 C CNN
	1    4200 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Counter_Clockwise J2
U 1 1 61CC283B
P 4200 2850
F 0 "J2" H 4250 3167 50  0000 C CNN
F 1 "Conn_02x03_Counter_Clockwise" H 4250 3076 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4200 2850 50  0001 C CNN
F 3 "~" H 4200 2850 50  0001 C CNN
	1    4200 2850
	1    0    0    -1  
$EndComp
Text Label 5850 2350 2    50   ~ 0
mol4
Text Label 5850 2250 2    50   ~ 0
mol3
Text Label 5850 2150 2    50   ~ 0
mol2
Text Label 5850 2050 2    50   ~ 0
mol1
Text Label 4000 2500 2    50   ~ 0
mol3
Text Label 4000 2400 2    50   ~ 0
mol2
Text Label 4000 2300 2    50   ~ 0
mol1
Text Label 5850 2950 2    50   ~ 0
mol10
Text Label 4000 2950 2    50   ~ 0
mol10
Text Label 5850 2850 2    50   ~ 0
mol9
Text Label 4500 2850 0    50   ~ 0
mol9
Text Label 5850 2750 2    50   ~ 0
mol8
Text Label 4500 2750 0    50   ~ 0
mol8
Text Label 5850 2650 2    50   ~ 0
mol7
Text Label 4500 2950 0    50   ~ 0
mol7
Text Label 5850 2550 2    50   ~ 0
mol6
Text Label 5850 2450 2    50   ~ 0
mol5
Text Label 4500 2300 0    50   ~ 0
mol5
Text Label 4500 2400 0    50   ~ 0
mol6
Text Label 4500 2500 0    50   ~ 0
mol4
$EndSCHEMATC
