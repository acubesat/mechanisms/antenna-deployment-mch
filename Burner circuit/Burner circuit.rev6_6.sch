EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ADM filler v0"
Date ""
Rev "v0"
Comp "SpaceDot - Panagiotis Bountzioukas"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:QCN-5D+ IC3
U 1 1 60B6E398
P 3650 3900
F 0 "IC3" H 4850 4165 50  0000 C CNN
F 1 "50Ohm" H 4850 4074 50  0001 C CNN
F 2 "SamacSys_Parts:QCN5D" H 5900 4000 50  0001 L CNN
F 3 "https://www.mouser.in/datasheet/2/1030/QCN-5D_2b-1701289.pdf" H 5900 3900 50  0001 L CNN
F 4 "Signal Conditioning LTCC 90 Hybrid, 330 - 580 MHz, 50 ohms" H 5900 3800 50  0001 L CNN "Description"
F 5 "1.068" H 5900 3700 50  0001 L CNN "Height"
F 6 "139-QCN-5D" H 5900 3600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Mini-Circuits/QCN-5D%2b?qs=xZ%2FP%252Ba9zWqai7gDmnIyxcA%3D%3D" H 5900 3500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Mini-Circuits" H 5900 3400 50  0001 L CNN "Manufacturer_Name"
F 9 "QCN-5D+" H 5900 3300 50  0001 L CNN "Manufacturer_Part_Number"
	1    3650 3900
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 60B73D70
P 4800 4400
F 0 "#PWR012" H 4800 4150 50  0001 C CNN
F 1 "GND" H 4805 4227 50  0000 C CNN
F 2 "" H 4800 4400 50  0001 C CNN
F 3 "" H 4800 4400 50  0001 C CNN
	1    4800 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 60B74046
P 3950 4400
F 0 "#PWR09" H 3950 4150 50  0001 C CNN
F 1 "GND" H 3955 4227 50  0000 C CNN
F 2 "" H 3950 4400 50  0001 C CNN
F 3 "" H 3950 4400 50  0001 C CNN
	1    3950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4000 4800 4000
Wire Wire Line
	4800 4000 4800 4300
Wire Wire Line
	3650 4000 3950 4000
Wire Wire Line
	7500 4000 7600 4000
Wire Wire Line
	7600 4000 7600 4300
Wire Wire Line
	7600 4300 5100 4300
Connection ~ 4800 4300
Wire Wire Line
	4800 4300 4800 4400
Wire Wire Line
	3950 4300 3950 4400
Wire Wire Line
	3950 4000 3950 4300
Connection ~ 3950 4300
Wire Wire Line
	1250 4000 1050 4000
Wire Wire Line
	1050 4000 1050 4300
Wire Wire Line
	1050 4300 3650 4300
$Comp
L power:GND #PWR013
U 1 1 60B7B37E
P 4900 2750
F 0 "#PWR013" H 4900 2500 50  0001 C CNN
F 1 "GND" H 4905 2577 50  0000 C CNN
F 2 "" H 4900 2750 50  0001 C CNN
F 3 "" H 4900 2750 50  0001 C CNN
	1    4900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2500 4900 2600
Text Notes 650  3450 0    50   ~ 0
elements counted clockwise
$Comp
L Device:R_Small R4
U 1 1 60B95620
P 3650 4200
F 0 "R4" H 3709 4246 50  0000 L CNN
F 1 "50Ω" V 3650 4150 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3650 4200 50  0001 C CNN
F 3 "~" H 3650 4200 50  0001 C CNN
	1    3650 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 60B95B4D
P 5100 4200
F 0 "R9" H 5159 4246 50  0000 L CNN
F 1 "50Ω" V 5100 4150 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5100 4200 50  0001 C CNN
F 3 "~" H 5100 4200 50  0001 C CNN
	1    5100 4200
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:QCN-5D+ IC4
U 1 1 60B6D5E5
P 5100 3900
F 0 "IC4" H 6300 4165 50  0000 C CNN
F 1 "QCN-5D+" H 6300 4074 50  0000 C CNN
F 2 "SamacSys_Parts:QCN5D" H 7350 4000 50  0001 L CNN
F 3 "https://www.mouser.in/datasheet/2/1030/QCN-5D_2b-1701289.pdf" H 7350 3900 50  0001 L CNN
F 4 "Signal Conditioning LTCC 90 Hybrid, 330 - 580 MHz, 50 ohms" H 7350 3800 50  0001 L CNN "Description"
F 5 "1.068" H 7350 3700 50  0001 L CNN "Height"
F 6 "139-QCN-5D" H 7350 3600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Mini-Circuits/QCN-5D%2b?qs=xZ%2FP%252Ba9zWqai7gDmnIyxcA%3D%3D" H 7350 3500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Mini-Circuits" H 7350 3400 50  0001 L CNN "Manufacturer_Name"
F 9 "QCN-5D+" H 7350 3300 50  0001 L CNN "Manufacturer_Part_Number"
	1    5100 3900
	1    0    0    -1  
$EndComp
Connection ~ 3650 4300
Wire Wire Line
	3650 4300 3950 4300
Connection ~ 5100 4300
Wire Wire Line
	5100 4300 4800 4300
$Comp
L SamacSys_Parts:JJCHLUJ100NCNMRTR S1
U 1 1 60B9AC82
P 1100 5200
F 0 "S1" H 1550 5465 50  0000 C CNN
F 1 "JJCHLUJ100NCNMRTR" H 1550 5374 50  0000 C CNN
F 2 "SamacSys_Parts:JJCHLUJ100NCNMRTR" H 1850 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/JJCHLUJ100NCNMRTR.pdf" H 1850 5200 50  0001 L CNN
F 4 "Detector Switches JJC Detector Switch 3.5x3.3x1 JB HORZ(L)" H 1850 5100 50  0001 L CNN "Description"
F 5 "1" H 1850 5000 50  0001 L CNN "Height"
F 6 "506-JJCHLUJ100NCNMR" H 1850 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity-Alcoswitch/JJCHLUJ100NCNMRTR?qs=%252BEew9%252B0nqrCPMEy1F0yZQw%3D%3D" H 1850 4800 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 1850 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "JJCHLUJ100NCNMRTR" H 1850 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    1100 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3900 4300 3900
$Comp
L power:GND #PWR03
U 1 1 60BB2FA5
P 2000 5500
F 0 "#PWR03" H 2000 5250 50  0001 C CNN
F 1 "GND" H 2005 5327 50  0000 C CNN
F 2 "" H 2000 5500 50  0001 C CNN
F 3 "" H 2000 5500 50  0001 C CNN
	1    2000 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5200 2000 5300
Connection ~ 2000 5300
Wire Wire Line
	2000 5300 2000 5500
Text GLabel 1000 5300 0    50   Input ~ 0
sw1
Text GLabel 1000 5200 0    50   Input ~ 0
Vcc
Wire Wire Line
	1100 5200 1000 5200
$Comp
L SamacSys_Parts:JJCHLUJ100NCNMRTR S2
U 1 1 60BB9029
P 2400 5200
F 0 "S2" H 2850 5465 50  0000 C CNN
F 1 "JJCHLUJ100NCNMRTR" H 2850 5374 50  0000 C CNN
F 2 "SamacSys_Parts:JJCHLUJ100NCNMRTR" H 3150 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/JJCHLUJ100NCNMRTR.pdf" H 3150 5200 50  0001 L CNN
F 4 "Detector Switches JJC Detector Switch 3.5x3.3x1 JB HORZ(L)" H 3150 5100 50  0001 L CNN "Description"
F 5 "1" H 3150 5000 50  0001 L CNN "Height"
F 6 "506-JJCHLUJ100NCNMR" H 3150 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity-Alcoswitch/JJCHLUJ100NCNMRTR?qs=%252BEew9%252B0nqrCPMEy1F0yZQw%3D%3D" H 3150 4800 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 3150 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "JJCHLUJ100NCNMRTR" H 3150 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    2400 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 60BB902F
P 3300 5500
F 0 "#PWR06" H 3300 5250 50  0001 C CNN
F 1 "GND" H 3305 5327 50  0000 C CNN
F 2 "" H 3300 5500 50  0001 C CNN
F 3 "" H 3300 5500 50  0001 C CNN
	1    3300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5200 3300 5300
Connection ~ 3300 5300
Wire Wire Line
	3300 5300 3300 5500
Text GLabel 2300 5300 0    50   Input ~ 0
sw2
Text GLabel 2300 5200 0    50   Input ~ 0
Vcc
Wire Wire Line
	2400 5200 2300 5200
$Comp
L SamacSys_Parts:JJCHLUJ100NCNMRTR S3
U 1 1 60BBA25D
P 3700 5200
F 0 "S3" H 4150 5465 50  0000 C CNN
F 1 "JJCHLUJ100NCNMRTR" H 4150 5374 50  0000 C CNN
F 2 "SamacSys_Parts:JJCHLUJ100NCNMRTR" H 4450 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/JJCHLUJ100NCNMRTR.pdf" H 4450 5200 50  0001 L CNN
F 4 "Detector Switches JJC Detector Switch 3.5x3.3x1 JB HORZ(L)" H 4450 5100 50  0001 L CNN "Description"
F 5 "1" H 4450 5000 50  0001 L CNN "Height"
F 6 "506-JJCHLUJ100NCNMR" H 4450 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity-Alcoswitch/JJCHLUJ100NCNMRTR?qs=%252BEew9%252B0nqrCPMEy1F0yZQw%3D%3D" H 4450 4800 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 4450 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "JJCHLUJ100NCNMRTR" H 4450 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    3700 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 60BBA263
P 4600 5450
F 0 "#PWR011" H 4600 5200 50  0001 C CNN
F 1 "GND" H 4605 5277 50  0000 C CNN
F 2 "" H 4600 5450 50  0001 C CNN
F 3 "" H 4600 5450 50  0001 C CNN
	1    4600 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 5200 4600 5300
Connection ~ 4600 5300
Wire Wire Line
	4600 5300 4600 5450
Text GLabel 3600 5300 0    50   Input ~ 0
sw3
Text GLabel 3600 5200 0    50   Input ~ 0
Vcc
Wire Wire Line
	3700 5200 3600 5200
$Comp
L SamacSys_Parts:JJCHLUJ100NCNMRTR S4
U 1 1 60BBB61C
P 5000 5200
F 0 "S4" H 5450 5465 50  0000 C CNN
F 1 "JJCHLUJ100NCNMRTR" H 5450 5374 50  0000 C CNN
F 2 "SamacSys_Parts:JJCHLUJ100NCNMRTR" H 5750 5300 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/JJCHLUJ100NCNMRTR.pdf" H 5750 5200 50  0001 L CNN
F 4 "Detector Switches JJC Detector Switch 3.5x3.3x1 JB HORZ(L)" H 5750 5100 50  0001 L CNN "Description"
F 5 "1" H 5750 5000 50  0001 L CNN "Height"
F 6 "506-JJCHLUJ100NCNMR" H 5750 4900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity-Alcoswitch/JJCHLUJ100NCNMRTR?qs=%252BEew9%252B0nqrCPMEy1F0yZQw%3D%3D" H 5750 4800 50  0001 L CNN "Mouser Price/Stock"
F 8 "TE Connectivity" H 5750 4700 50  0001 L CNN "Manufacturer_Name"
F 9 "JJCHLUJ100NCNMRTR" H 5750 4600 50  0001 L CNN "Manufacturer_Part_Number"
	1    5000 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 60BBB622
P 5900 5450
F 0 "#PWR019" H 5900 5200 50  0001 C CNN
F 1 "GND" H 5905 5277 50  0000 C CNN
F 2 "" H 5900 5450 50  0001 C CNN
F 3 "" H 5900 5450 50  0001 C CNN
	1    5900 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5200 5900 5300
Connection ~ 5900 5300
Wire Wire Line
	5900 5300 5900 5450
Text GLabel 4900 5300 0    50   Input ~ 0
sw4
Text GLabel 4900 5200 0    50   Input ~ 0
Vcc
Wire Wire Line
	5000 5200 4900 5200
$Comp
L Device:R_Small R1
U 1 1 60BBF5AB
P 1100 5400
F 0 "R1" H 1150 5400 50  0000 L CNN
F 1 "10K" V 1100 5350 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1100 5400 50  0001 C CNN
F 3 "~" H 1100 5400 50  0001 C CNN
	1    1100 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5300 1000 5300
Connection ~ 1100 5300
$Comp
L power:GND #PWR01
U 1 1 60BC1B14
P 1100 5500
F 0 "#PWR01" H 1100 5250 50  0001 C CNN
F 1 "GND" H 1105 5327 50  0000 C CNN
F 2 "" H 1100 5500 50  0001 C CNN
F 3 "" H 1100 5500 50  0001 C CNN
	1    1100 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 60BC2AF2
P 2400 5400
F 0 "R3" H 2450 5400 50  0000 L CNN
F 1 "10K" V 2400 5350 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2400 5400 50  0001 C CNN
F 3 "~" H 2400 5400 50  0001 C CNN
	1    2400 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60BC2FE8
P 2400 5500
F 0 "#PWR04" H 2400 5250 50  0001 C CNN
F 1 "GND" H 2405 5327 50  0000 C CNN
F 2 "" H 2400 5500 50  0001 C CNN
F 3 "" H 2400 5500 50  0001 C CNN
	1    2400 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5300 2300 5300
Connection ~ 2400 5300
$Comp
L Device:R_Small R8
U 1 1 60BC4421
P 5000 5400
F 0 "R8" H 5050 5400 50  0000 L CNN
F 1 "10K" V 5000 5350 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5000 5400 50  0001 C CNN
F 3 "~" H 5000 5400 50  0001 C CNN
	1    5000 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5300 3700 5300
Connection ~ 3700 5300
$Comp
L power:GND #PWR07
U 1 1 60BC5215
P 3700 5500
F 0 "#PWR07" H 3700 5250 50  0001 C CNN
F 1 "GND" H 3705 5327 50  0000 C CNN
F 2 "" H 3700 5500 50  0001 C CNN
F 3 "" H 3700 5500 50  0001 C CNN
	1    3700 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 60BC5E7A
P 5000 5500
F 0 "#PWR014" H 5000 5250 50  0001 C CNN
F 1 "GND" H 5005 5327 50  0000 C CNN
F 2 "" H 5000 5500 50  0001 C CNN
F 3 "" H 5000 5500 50  0001 C CNN
	1    5000 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5300 4900 5300
Connection ~ 5000 5300
Text GLabel 9300 6000 3    50   Input ~ 0
sw1
Text GLabel 9300 5100 1    50   Input ~ 0
Vcc
Text GLabel 9500 6000 3    50   Input ~ 0
sw2
Text GLabel 9600 6000 3    50   Input ~ 0
sw3
Text GLabel 9700 6000 3    50   Input ~ 0
sw4
$Comp
L power:GND #PWR021
U 1 1 60BC8013
P 9900 5100
F 0 "#PWR021" H 9900 4850 50  0001 C CNN
F 1 "GND" H 9905 4927 50  0000 C CNN
F 2 "" H 9900 5100 50  0001 C CNN
F 3 "" H 9900 5100 50  0001 C CNN
	1    9900 5100
	1    0    0    -1  
$EndComp
Text GLabel 1550 6600 2    50   Input ~ 0
Vcc
$Comp
L Device:R_Small R2
U 1 1 60BCB7C9
P 1250 6800
F 0 "R2" H 1300 6800 50  0000 L CNN
F 1 "10K" V 1250 6750 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1250 6800 50  0001 C CNN
F 3 "~" H 1250 6800 50  0001 C CNN
	1    1250 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 60BCC0A0
P 1250 6900
F 0 "#PWR02" H 1250 6650 50  0001 C CNN
F 1 "GND" H 1255 6727 50  0000 C CNN
F 2 "" H 1250 6900 50  0001 C CNN
F 3 "" H 1250 6900 50  0001 C CNN
	1    1250 6900
	1    0    0    -1  
$EndComp
Text GLabel 1550 6700 2    50   Input ~ 0
photopin
Wire Wire Line
	1550 6600 1250 6600
Wire Wire Line
	1550 6700 1250 6700
Text GLabel 9400 6000 3    50   Input ~ 0
photopin
$Comp
L SamacSys_Parts:BD2069FJ-MGE2 IC2
U 1 1 60BCF654
P 2900 6750
F 0 "IC2" H 3450 7015 50  0000 C CNN
F 1 "BD2069FJ-MGE2" H 3450 6924 50  0000 C CNN
F 2 "SamacSys_Parts:SOIC127P600X165-8N" H 3850 6850 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/2/BD2069FJ-MGE2.pdf" H 3850 6750 50  0001 L CNN
F 4 "Power Switch ICs - Power Distribution 2.4A 2.7-5.5V 2ch Crnt Limit H. Side" H 3850 6650 50  0001 L CNN "Description"
F 5 "1.65" H 3850 6550 50  0001 L CNN "Height"
F 6 "755-BD2069FJ-MGE2" H 3850 6450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/BD2069FJ-MGE2?qs=WHeKGGBt0W8aeBPIwgyoAQ%3D%3D" H 3850 6350 50  0001 L CNN "Mouser Price/Stock"
F 8 "ROHM Semiconductor" H 3850 6250 50  0001 L CNN "Manufacturer_Name"
F 9 "BD2069FJ-MGE2" H 3850 6150 50  0001 L CNN "Manufacturer_Part_Number"
	1    2900 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 60BD01B9
P 4150 6750
F 0 "C2" H 4050 6850 50  0000 L CNN
F 1 "220uF" H 4050 6700 35  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4150 6750 50  0001 C CNN
F 3 "~" H 4150 6750 50  0001 C CNN
	1    4150 6750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C1
U 1 1 60BD07B9
P 2600 6750
F 0 "C1" H 2508 6704 50  0000 R CNN
F 1 "1uF" H 2508 6795 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2600 6750 50  0001 C CNN
F 3 "~" H 2600 6750 50  0001 C CNN
	1    2600 6750
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 60BD09E7
P 4300 6550
F 0 "C3" H 4300 6650 50  0000 L CNN
F 1 "220uF" H 4200 6500 35  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4300 6550 50  0001 C CNN
F 3 "~" H 4300 6550 50  0001 C CNN
	1    4300 6550
	1    0    0    -1  
$EndComp
Text GLabel 2300 6850 0    50   Input ~ 0
Vcc
Wire Wire Line
	2900 6850 2600 6850
Connection ~ 2600 6850
Wire Wire Line
	2600 6850 2300 6850
$Comp
L power:GND #PWR05
U 1 1 60BD208C
P 2600 6350
F 0 "#PWR05" H 2600 6100 50  0001 C CNN
F 1 "GND" H 2605 6177 50  0000 C CNN
F 2 "" H 2600 6350 50  0001 C CNN
F 3 "" H 2600 6350 50  0001 C CNN
	1    2600 6350
	-1   0    0    1   
$EndComp
Wire Wire Line
	2600 6350 2600 6650
Wire Wire Line
	2600 6350 2900 6350
Connection ~ 2600 6350
Wire Wire Line
	2900 6750 2900 6350
Connection ~ 2900 6350
Wire Wire Line
	2900 6350 4150 6350
Wire Wire Line
	4000 6850 4150 6850
Wire Wire Line
	4150 6350 4150 6650
Wire Wire Line
	4300 6350 4150 6350
Connection ~ 4150 6350
$Comp
L Device:R_Small R5
U 1 1 60BC4659
P 3700 5400
F 0 "R5" H 3750 5400 50  0000 L CNN
F 1 "10K" V 3700 5350 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3700 5400 50  0001 C CNN
F 3 "~" H 3700 5400 50  0001 C CNN
	1    3700 5400
	1    0    0    -1  
$EndComp
Text GLabel 4550 7050 2    50   Input ~ 0
Vcc
Text GLabel 4550 6850 2    50   Input ~ 0
out1
Text GLabel 4550 6950 2    50   Input ~ 0
out2
Wire Wire Line
	4300 6350 4300 6450
Wire Wire Line
	4300 6650 4300 6950
Wire Wire Line
	4000 6950 4300 6950
Connection ~ 4300 6950
Wire Wire Line
	4300 6950 4550 6950
Wire Wire Line
	4550 6850 4150 6850
Connection ~ 4150 6850
Wire Wire Line
	4550 7050 4200 7050
Text GLabel 4550 6750 2    50   Input ~ 0
Vcc
$Comp
L Device:R_Small R6
U 1 1 60BDC6EA
P 4100 7050
F 0 "R6" V 4200 7000 50  0000 L CNN
F 1 "10K" V 4100 7000 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4100 7050 50  0001 C CNN
F 3 "~" H 4100 7050 50  0001 C CNN
	1    4100 7050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 60BE9E28
P 4450 6750
F 0 "R7" V 4550 6700 50  0000 L CNN
F 1 "10K" V 4450 6700 35  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4450 6750 50  0001 C CNN
F 3 "~" H 4450 6750 50  0001 C CNN
	1    4450 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	4350 6750 4000 6750
Text GLabel 2300 6950 0    50   Input ~ 0
en1
Text GLabel 2300 7050 0    50   Input ~ 0
en2
Wire Wire Line
	2900 6950 2300 6950
Wire Wire Line
	2900 7050 2300 7050
Text GLabel 9500 5100 1    50   Input ~ 0
en1
Text GLabel 9400 5100 1    50   Input ~ 0
en2
$Comp
L Device:R_Small R10
U 1 1 60BEF80B
P 5100 6900
F 0 "R10" H 5159 6946 50  0000 L CNN
F 1 "10Ω" H 5159 6855 50  0000 L CNN
F 2 "SamacSys_Parts:RESAD1526W69L610D229" H 5100 6900 50  0001 C CNN
F 3 "~" H 5100 6900 50  0001 C CNN
	1    5100 6900
	1    0    0    -1  
$EndComp
Text GLabel 5100 6650 1    50   Input ~ 0
out1
Text GLabel 5400 6650 1    50   Input ~ 0
out2
$Comp
L power:GND #PWR015
U 1 1 60BF4C5E
P 5100 7200
F 0 "#PWR015" H 5100 6950 50  0001 C CNN
F 1 "GND" H 5105 7027 50  0000 C CNN
F 2 "" H 5100 7200 50  0001 C CNN
F 3 "" H 5100 7200 50  0001 C CNN
	1    5100 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 7000 5100 7200
Wire Wire Line
	5100 6650 5100 6800
$Comp
L Device:R_Small R11
U 1 1 60BF8C13
P 5400 6900
F 0 "R11" H 5459 6946 50  0000 L CNN
F 1 "10Ω" H 5459 6855 50  0000 L CNN
F 2 "SamacSys_Parts:RESAD1526W69L610D229" H 5400 6900 50  0001 C CNN
F 3 "~" H 5400 6900 50  0001 C CNN
	1    5400 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 6650 5400 6800
$Comp
L power:GND #PWR016
U 1 1 60BFA346
P 5400 7200
F 0 "#PWR016" H 5400 6950 50  0001 C CNN
F 1 "GND" H 5405 7027 50  0000 C CNN
F 2 "" H 5400 7200 50  0001 C CNN
F 3 "" H 5400 7200 50  0001 C CNN
	1    5400 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 7000 5400 7200
$Comp
L Device:R_Small R12
U 1 1 60BFC6C7
P 5750 6900
F 0 "R12" H 5809 6946 50  0000 L CNN
F 1 "10Ω" H 5809 6855 50  0000 L CNN
F 2 "SamacSys_Parts:RESAD1526W69L610D229" H 5750 6900 50  0001 C CNN
F 3 "~" H 5750 6900 50  0001 C CNN
	1    5750 6900
	1    0    0    -1  
$EndComp
Text GLabel 5750 6650 1    50   Input ~ 0
out1
Text GLabel 6050 6650 1    50   Input ~ 0
out2
$Comp
L power:GND #PWR018
U 1 1 60BFC6CF
P 5750 7200
F 0 "#PWR018" H 5750 6950 50  0001 C CNN
F 1 "GND" H 5755 7027 50  0000 C CNN
F 2 "" H 5750 7200 50  0001 C CNN
F 3 "" H 5750 7200 50  0001 C CNN
	1    5750 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 7000 5750 7200
Wire Wire Line
	5750 6650 5750 6800
$Comp
L Device:R_Small R13
U 1 1 60BFC6D7
P 6050 6900
F 0 "R13" H 6109 6946 50  0000 L CNN
F 1 "10Ω" H 6109 6855 50  0000 L CNN
F 2 "SamacSys_Parts:RESAD1526W69L610D229" H 6050 6900 50  0001 C CNN
F 3 "~" H 6050 6900 50  0001 C CNN
	1    6050 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 6650 6050 6800
$Comp
L power:GND #PWR020
U 1 1 60BFC6DE
P 6050 7200
F 0 "#PWR020" H 6050 6950 50  0001 C CNN
F 1 "GND" H 6055 7027 50  0000 C CNN
F 2 "" H 6050 7200 50  0001 C CNN
F 3 "" H 6050 7200 50  0001 C CNN
	1    6050 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 7000 6050 7200
Text Notes 3950 6100 0    59   Italic 12
Burner Circuit
Wire Notes Line
	6350 5950 6350 7600
Wire Notes Line
	6350 7600 2050 7600
Wire Notes Line
	2050 7600 2050 5950
Wire Notes Line
	2050 5950 6350 5950
Wire Notes Line
	600  5950 600  7600
Wire Notes Line
	600  7600 1950 7600
Wire Notes Line
	1950 7600 1950 5950
Wire Notes Line
	1950 5950 600  5950
Text Notes 1000 6100 0    59   Italic 12
Photodiode
Wire Notes Line
	6350 4750 600  4750
Wire Notes Line
	600  5800 6350 5800
Wire Notes Line
	6350 4750 6350 5800
Wire Notes Line
	600  4750 600  5800
Text Notes 3200 4900 0    59   Italic 12
Dep. Switches
Wire Notes Line
	8050 4650 600  4650
Wire Notes Line
	600  4650 600  1950
Wire Notes Line
	600  1950 8050 1950
Wire Notes Line
	8050 1950 8050 4650
Text Notes 4200 2150 0    59   Italic 12
RF Circuitry
Wire Notes Line
	8250 4650 8250 6450
Wire Notes Line
	8250 6450 10750 6450
Wire Notes Line
	10750 6450 10750 4650
Wire Notes Line
	10750 4650 8250 4650
Text Notes 9350 4800 0    59   Italic 12
Connector
$Comp
L SamacSys_Parts:SBTCJ-1WX+ FL1
U 1 1 60BA5ADF
P 4500 2400
F 0 "FL1" H 5250 2665 50  0000 C CNN
F 1 "SBTCJ-1WX+" H 5250 2574 50  0000 C CNN
F 2 "SamacSys_Parts:SBTCJ1WX" H 5850 2500 50  0001 L CNN
F 3 "https://www.minicircuits.com/pdfs/SBTCJ-1WX+.pdf" H 5850 2400 50  0001 L CNN
F 4 "CORE & WIRE 180 HYBRID, 1 - 750" H 5850 2300 50  0001 L CNN "Description"
F 5 "3.81" H 5850 2200 50  0001 L CNN "Height"
F 6 "" H 5850 2100 50  0001 L CNN "Mouser Part Number"
F 7 "" H 5850 2000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Mini-Circuits" H 5850 1900 50  0001 L CNN "Manufacturer_Name"
F 9 "SBTCJ-1WX+" H 5850 1800 50  0001 L CNN "Manufacturer_Part_Number"
	1    4500 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3900 4500 3900
$Comp
L power:GND #PWR010
U 1 1 60BB18CF
P 4400 3900
F 0 "#PWR010" H 4400 3650 50  0001 C CNN
F 1 "GND" H 4405 3727 50  0000 C CNN
F 2 "" H 4400 3900 50  0001 C CNN
F 3 "" H 4400 3900 50  0001 C CNN
	1    4400 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 2400 4500 2400
$Comp
L power:GND #PWR08
U 1 1 60BBF1AC
P 3950 2650
F 0 "#PWR08" H 3950 2400 50  0001 C CNN
F 1 "GND" H 3955 2477 50  0000 C CNN
F 2 "" H 3950 2650 50  0001 C CNN
F 3 "" H 3950 2650 50  0001 C CNN
	1    3950 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2400 3950 2400
Wire Wire Line
	3950 2400 3950 2650
$Comp
L SamacSys_Parts:MMCX-R-PC_40_ J1
U 1 1 60BCB7E2
P 4900 2400
F 0 "J1" H 5300 1935 50  0000 C CNN
F 1 "MMCX-R-PC_40_" H 5300 2026 50  0000 C CNN
F 2 "SamacSys_Parts:MMCXRPC40" H 5550 2500 50  0001 L CNN
F 3 "https://www.hirose.com/product/document?clcode=CL0339-0005-8-40&productname=MMCX-R-PC(40)&series=MMCX&documenttype=2DDrawing&lang=en&documentid=0000965555" H 5550 2400 50  0001 L CNN
F 4 "RF Connectors / Coaxial Connectors MINI PUSH-ON CONN RECEPT STR" H 5550 2300 50  0001 L CNN "Description"
F 5 "4.6" H 5550 2200 50  0001 L CNN "Height"
F 6 "798-MMCX-R-PC(40)" H 5550 2100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Hirose-Connector/MMCX-R-PC40?qs=XQjbzJWzFPWEezvy%2FkLdHw%3D%3D" H 5550 2000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Hirose" H 5550 1900 50  0001 L CNN "Manufacturer_Name"
F 9 "MMCX-R-PC(40)" H 5550 1800 50  0001 L CNN "Manufacturer_Part_Number"
	1    4900 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 60BD4B2D
P 5700 2750
F 0 "#PWR017" H 5700 2500 50  0001 C CNN
F 1 "GND" H 5705 2577 50  0000 C CNN
F 2 "" H 5700 2750 50  0001 C CNN
F 3 "" H 5700 2750 50  0001 C CNN
	1    5700 2750
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:504050-1091 J2
U 1 1 60BD7036
P 9700 5100
F 0 "J2" V 10104 5228 50  0000 L CNN
F 1 "504050-1091" V 10195 5228 50  0000 L CNN
F 2 "SamacSys_Parts:504050-1091" H 10450 5200 50  0001 L CNN
F 3 "" H 10450 5100 50  0001 L CNN
F 4 "Pico-Lock SMT Wire-to-Board Header 10way Molex 504050 Series, 1.5mm Pitch 10 Way 1 Row Right Angle PCB Header, SMT Termination, 3A" H 10450 5000 50  0001 L CNN "Description"
F 5 "" H 10450 4900 50  0001 L CNN "Height"
F 6 "538-504050-1091" H 10450 4800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/504050-1091?qs=bvCPb%252BE7ys3Pdh2ki4M4Kw%3D%3D" H 10450 4700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 10450 4600 50  0001 L CNN "Manufacturer_Name"
F 9 "504050-1091" H 10450 4500 50  0001 L CNN "Manufacturer_Part_Number"
	1    9700 5100
	0    1    1    0   
$EndComp
Wire Wire Line
	9900 5100 9700 5100
$Comp
L dk_Test-Points:5001 TP3
U 1 1 60C0C08B
P 7500 4250
F 0 "TP3" H 7550 4297 50  0000 L CNN
F 1 "5001" H 7500 4150 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 7700 4450 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 7700 4550 60  0001 L CNN
F 4 "36-5001-ND" H 7700 4650 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 7700 4750 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 7700 4850 60  0001 L CNN "Category"
F 7 "Test Points" H 7700 4950 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 7700 5050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 7700 5150 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 7700 5250 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 7700 5350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7700 5450 60  0001 L CNN "Status"
	1    7500 4250
	1    0    0    -1  
$EndComp
$Comp
L dk_Test-Points:5001 TP4
U 1 1 60C0C9CE
P 7500 3750
F 0 "TP4" H 7450 3797 50  0000 R CNN
F 1 "5001" H 7500 3650 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 7700 3950 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 7700 4050 60  0001 L CNN
F 4 "36-5001-ND" H 7700 4150 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 7700 4250 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 7700 4350 60  0001 L CNN "Category"
F 7 "Test Points" H 7700 4450 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 7700 4550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 7700 4650 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 7700 4750 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 7700 4850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7700 4950 60  0001 L CNN "Status"
	1    7500 3750
	-1   0    0    1   
$EndComp
$Comp
L dk_Test-Points:5001 TP1
U 1 1 60C0CD41
P 1250 4250
F 0 "TP1" H 1300 4297 50  0000 L CNN
F 1 "5001" H 1250 4150 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 1450 4450 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1450 4550 60  0001 L CNN
F 4 "36-5001-ND" H 1450 4650 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 1450 4750 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 1450 4850 60  0001 L CNN "Category"
F 7 "Test Points" H 1450 4950 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1450 5050 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 1450 5150 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 1450 5250 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 1450 5350 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1450 5450 60  0001 L CNN "Status"
	1    1250 4250
	1    0    0    -1  
$EndComp
$Comp
L dk_Test-Points:5001 TP2
U 1 1 60C0E128
P 1250 3750
F 0 "TP2" H 1200 3797 50  0000 R CNN
F 1 "5001" H 1250 3650 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 1450 3950 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1450 4050 60  0001 L CNN
F 4 "36-5001-ND" H 1450 4150 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 1450 4250 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 1450 4350 60  0001 L CNN "Category"
F 7 "Test Points" H 1450 4450 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1450 4550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 1450 4650 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 1450 4750 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 1450 4850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1450 4950 60  0001 L CNN "Status"
	1    1250 3750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1250 3850 1250 3900
Wire Wire Line
	1250 4100 1250 4150
Wire Wire Line
	7500 3850 7500 3900
Wire Wire Line
	7500 4100 7500 4150
Wire Wire Line
	5700 2500 5700 2750
Wire Wire Line
	4900 2600 4900 2750
Connection ~ 4900 2600
Wire Wire Line
	5700 2500 5700 2400
Connection ~ 5700 2500
$Comp
L dk_Test-Points:5001 PHp1
U 1 1 60BE5D99
P 1550 6800
F 0 "PHp1" H 1650 6700 50  0000 R CNN
F 1 "5001" H 1550 6700 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 1750 7000 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1750 7100 60  0001 L CNN
F 4 "36-5001-ND" H 1750 7200 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 1750 7300 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 1750 7400 60  0001 L CNN "Category"
F 7 "Test Points" H 1750 7500 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1750 7600 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 1750 7700 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 1750 7800 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 1750 7900 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1750 8000 60  0001 L CNN "Status"
	1    1550 6800
	1    0    0    -1  
$EndComp
$Comp
L dk_Test-Points:5001 PHv1
U 1 1 60BE7A4A
P 1550 6500
F 0 "PHv1" H 1500 6450 50  0000 R CNN
F 1 "5001" H 1550 6400 50  0001 C CNN
F 2 "digikey-footprints:Test_Point_D1.02mm" H 1750 6700 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1750 6800 60  0001 L CNN
F 4 "36-5001-ND" H 1750 6900 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 1750 7000 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 1750 7100 60  0001 L CNN "Category"
F 7 "Test Points" H 1750 7200 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 1750 7300 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 1750 7400 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 1750 7500 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 1750 7600 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1750 7700 60  0001 L CNN "Status"
	1    1550 6500
	-1   0    0    1   
$EndComp
Connection ~ 1250 6700
Text Notes 650  7550 0    50   ~ 0
Daughterboard for the Photodiode\nto be later used
$Comp
L SamacSys_Parts:SFH_2430-Z IC1
U 1 1 60BEE01A
P 1250 6700
F 0 "IC1" H 1542 6335 50  0000 C CNN
F 1 "SFH_2430-Z" H 1542 6426 50  0000 C CNN
F 2 "SamacSys_Parts:SFH2430Z" H 1900 6800 50  0001 L CNN
F 3 "https://media.osram.info/media/resource/hires/osram-dam-2496025/SFH%202430.pdf" H 1900 6700 50  0001 L CNN
F 4 "Photodiodes PHOTODIODE (ALS) SMT Silicon Photodiode with V Characteristic" H 1900 6600 50  0001 L CNN "Description"
F 5 "1.3" H 1900 6500 50  0001 L CNN "Height"
F 6 "720-SFH2430-Z" H 1900 6400 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/OSRAM-Opto-Semiconductors/SFH-2430-Z?qs=K5ta8V%252BWhtZplGU0%252BI7tvA%3D%3D" H 1900 6300 50  0001 L CNN "Mouser Price/Stock"
F 8 "OSRAM" H 1900 6200 50  0001 L CNN "Manufacturer_Name"
F 9 "SFH 2430-Z" H 1900 6100 50  0001 L CNN "Manufacturer_Part_Number"
	1    1250 6700
	-1   0    0    1   
$EndComp
$EndSCHEMATC
