# Documentation

This directory contains the technical notes produced during the development and testing of the mechanism:
- Resistor Burning Notes
- Refurbishment Procedures

For detailed documentation of the mechanism's design refer to [link]

For the environmental test specification and report documents refer to [link]